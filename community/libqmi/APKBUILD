# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=libqmi
pkgver=1.24.8
pkgrel=1
pkgdesc="QMI modem protocol helper library"
url="https://www.freedesktop.org/wiki/Software/libqmi"
arch="all"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
makedepends="glib-dev gtk-doc libgudev-dev libmbim-dev linux-headers python3"
subpackages="$pkgname-dev $pkgname-doc"
source="https://www.freedesktop.org/software/libqmi/libqmi-$pkgver.tar.xz
	0001-qmi-device-support-and-detect-smdpkt-managed-QMI-con.patch
	0002-qmi-device-detect-rpmsg-control-ports.patch"

# Tests fail
[ "$CARCH" = s390x ] && options="$options !check"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--enable-mbim-qmux \
		--enable-more-warnings=yes
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b60ac355792373afbb202b2791c641468b48a1e54e5f34336fe37fa799c24ac566d6070ba4faaaf6b5693ae2bb5cc75c1240c7f2ee04c8e35eeeb66ec2677093  libqmi-1.24.8.tar.xz
2fb1f8532aaa734619bd4f24679b96042e1ea05367137cc7d40d2140ee84e80acd10d04557e49c64bfdb7c3b107e61a12e62d99440a4188ba1a7510b3f2000ea  0001-qmi-device-support-and-detect-smdpkt-managed-QMI-con.patch
c758e38fb9480c1806e12f433432e0cf461319dab94c9040695725f8d3577432306c97d6d783b3e9fd46ef2c6cf879243c7d0db89eb62baad847bd13ed0bd727  0002-qmi-device-detect-rpmsg-control-ports.patch"
