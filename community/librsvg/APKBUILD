# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=librsvg
pkgver=2.48.1
pkgrel=0
pkgdesc="SAX-based renderer for SVG files into a GdkPixbuf"
url="https://wiki.gnome.org/Projects/LibRsvg"
arch="all !s390x" # rust
license="LGPL-2.1-or-later"
options="!check" # Failing
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg $pkgname-lang"
makedepends="$depends_dev bzip2-dev cairo-dev glib-dev cargo rust vala
	gobject-introspection-dev gtk+3.0-dev libcroco-dev libgsf-dev"
source="https://download.gnome.org/sources/librsvg/${pkgver%.*}/librsvg-$pkgver.tar.xz"

# secfixes:
#   2.46.2-r0:
#     - CVE-2019-20446

build() {
	export RUSTFLAGS="$RUSTFLAGS -C debuginfo=2"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libexecdir=/usr/lib/$pkgname \
		--disable-static \
		--enable-vala
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="cd5965d10239b537d44fe874a3c9c475a01e884786a31dfc41351e642af812e21604a636c2282d9202f65eb75ae8e5d593a972b4679cbfc00d2fcd96fcb19918  librsvg-2.48.1.tar.xz"
