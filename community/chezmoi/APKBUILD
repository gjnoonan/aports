# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=chezmoi
pkgver=1.7.18
pkgrel=0
pkgdesc="Manage your dotfiles across multiple machines, securely."
url="https://www.chezmoi.io/"
arch="all"
license="MIT"
makedepends="go"
options="!check chmod-clean" # no test suite
subpackages="
	$pkgname-doc
	$pkgname-bash-completion:bashcomp:noarch
	$pkgname-zsh-completion:zshcomp:noarch
	$pkgname-fish-completion:fishcomp:noarch
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/twpayne/chezmoi/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	go build \
		-ldflags "
		-X \"main.version=$pkgver\" \
		-X \"main.date=$(date --utc +%Y-%M-%dT%H:%I:%SZ)\" \
		-X \"github.com/twpayne/chezmoi/cmd.DocsDir=/usr/share/doc/chezmoi/\"
		" \
		-tags noupgrade \
		-tags noembeddocs
	make completions
}

package() {
	install -Dm0755 chezmoi "$pkgdir"/usr/bin/chezmoi
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/chezmoi"
	cp "$builddir/docs/"* "$subpkgdir/usr/share/doc/chezmoi"
}

bashcomp() {
	pkgdesc="additional scripts for bash like shell completion"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	cd "$builddir"
	install -Dm0644 completions/chezmoi-completion.bash \
		"$subpkgdir"/usr/share/bash-completion/completions/$pkgname
}

zshcomp() {
	pkgdesc="additional scripts for zsh like shell completion"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	cd "$builddir"
	install -Dm0644 completions/chezmoi.zsh \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

fishcomp() {
	pkgdesc="additional scripts for fish like shell completion"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel fish"

	cd "$builddir"
	install -Dm0644 completions/chezmoi.fish \
		"$subpkgdir"/usr/share/fish/site-functions/_$pkgname
}

cleanup_srcdir() {
	go clean -modcache
	default_cleanup_srcdir
}

sha512sums="c5f11b7710c0035caab4fd0cc96d09e640d7bd0c83161f036d74defd41ee6daa1efbdc40f2d966fab905717ec17d444ed78ea10c1e476969a0c14455f62968df  chezmoi-1.7.18.tar.gz"
